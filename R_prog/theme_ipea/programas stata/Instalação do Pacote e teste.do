* Instalação do Pacote de Criação de Gráficos

net inst brewscheme, from("http://wbuchanan.github.io/brewscheme/") replace

* Teste de instalação da biblioteca
libbrewscheme

ado, find(brewscheme)
* Package package brewscheme from http://wbuchanan.github.io/brewscheme 'BREWSCHEME': a toolkit for data visualizations in Stata.

sysdir

personal [dir]

personal dir

* Alterando o Caminho ado\personal
sysdir set PERSONAL "D:\Users\b240174\personal"

mkdir C:/ado/personal 

* Criando o diretório Personal no ado
mkdir D:/Users/b240174/personal\

adopath

* Teste instalação brewscheme

brewcolordb, rep

brewscheme, scheme(set1) allst(set1) allc(5) allsat(80)

brewscheme, scheme(mixed1) scatst(set1) barst(pastel1) somest(brbg)

brewscheme, scheme(set1) allst(set1) allc(5) allsat(80)

brewscheme, scheme(onecolorex1) allsty(ggplot2)
