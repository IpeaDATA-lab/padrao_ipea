// Aplicação do Scheme Ipea para - Stata

// Alterando o Caminho ado\personal
sysdir set PERSONAL "D:\Users\b240174\personal"

//Conferindo Caminho
adopath

// Gerando a Paleta Ipea
brewmeta blues, colorid(3) colors(9)


// Gráfico1
// Load the data ipea
use "\\SBSB2\dpti\Usuarios\Lucas Mation\theme_ipea\dados\base_grafico1.dta", clear

// Default do Stata para gráficos de barra

 tw bar Populacao Ano


// Gerando o primeiro scheme para o gráfico 1

brewscheme, scheme(ipea_bar) allstyle(mdepoint) allcolor(3) 

//Gráfico 1 Ipea - Scheme

tw bar Populacao Ano, scheme(ipea_bar)  yscale(range(0 10) titlegap(1)) xscale(range(2000 2050) titlegap(5)) xtitle("Ano") ytitle("População") ylabel(0(1)10) xlabel(2000(5)2050)

//Gráfico 2 
// Load the data ipea
use "\\SBSB2\dpti\Usuarios\Lucas Mation\theme_ipea\dados\base_grafico2.dta", clear

// Default do Stata para gráficos de pizza
graph pie Freq, over(Assunto) plabel(_all percent, color(black) orientation(vertical) format(%9,0f)) caption(, bmargin(zero)) legend(on) clegend(on) plegend(on)

// Gráfico 2 IPea - Scheme
// Gerando o scheme para o gráfico 2

brewscheme, scheme(ipea_pie) allstyle(blues) allcolors(5)

// Gráfico 2 Ipea - Scheme

graph pie Freq, over(Assunto) plabel(_all percent, color(black) orientation(vertical) format(%9,0f)) caption(, bmargin(zero)) legend(on) scheme(ipea_pie)

//Gráfico 3 
// Load the data ipea 
use "\\SBSB2\dpti\Usuarios\Lucas Mation\theme_ipea\dados\base_grafico3.dta", clear

// Default do stata para gráficos de séries temporais
twoway (line Populacao Ano)

// Gerando o scheme par o gráfico 3
brewscheme, scheme(ipea_line) allstyle(mdepoint) allcolors(3)

// Gráfico 3 Ipea - Scheme

tw (line Populacao Ano), scheme(ipea_line) xscale(range(1 65) titlegap(5))  xtitle("Ano") ytitle("População") ylabel(2.5(0.5)0) xlabel(1(10)65)

//Gráfico 5
// Load the data ipea
use "\\SBSB2\dpti\Usuarios\Lucas Mation\theme_ipea\dados\base_grafico5.dta"

// Default do Stata para gráficos acumulados
twoway (area Exabytes Ano)

// Gráfico 5 Ipea - Scheme
// Gerando o scheme para o gráfico 5

brewscheme, scheme(ipea_area) allstyle(mdepoint) allcolors(3)

// Gráfico 5 Ipea - Scheme

tw area Exabytes Ano, scheme(ipea_area) xtitle("Ano")  ylabel(0(10000)40000) xlabel(2009(1)2020)


//Gráfico 6 
// Load the data ipea
use "\\SBSB2\dpti\Usuarios\Lucas Mation\theme_ipea\dados\base_grafico6.dta", clear

// Default do Stata para gráficos de séries temporais
twoway (rline Mundo EU Ano), legend(on)

// Gráfico 6 Ipea - Scheme
// Gerando o scheme para o gráfico 6

brewscheme, scheme(ipea_line) allstyle(mdepoint) allcolors(3)

// Gráfico 6 Ipea - Scheme

tw (rline Mundo EU Ano), scheme(ipea_line) legend(on) xtitle("Ano") ytitle("") ylabel(-6(1)6) xlabel(2004(1)2013) 


//Gráfico 7
// Load the data ipea
use "\\SBSB2\dpti\Usuarios\Lucas Mation\theme_ipea\dados\base_grafico7.dta", clear

// Default do stata para gráficos de barras horizontais
twoway (bar petroleo pais, horizontal)

// Gerando o scheme para o gráfico 7
brewscheme, scheme(ipea_barh) allstyle(mdepoint) allcolors(3)

// Gráfico 7 Ipea - Scheme
tw (bar petroleo pais, horizontal), scheme(ipea_barh) xtitle("Petróleo") ytitle("País") ylabel(1(1)28) xlabel(-80(40)120)

// Gráfico 8 - falta fazer
// Load the data ipea
use "\\SBSB2\dpti\Usuarios\Lucas Mation\theme_ipea\dados\base_grafico8.dta", clear

// Default do stata para gráficos de categorias e barras horizontais
tw (rbar final inicio pais, horizontal)

// Gerando o scheme para o gráfico 8
brewscheme, scheme(ipea_barhc) allstyle(mdepoint) allcolors(3)

// Gráfico 8 Ipea - Scheme
tw (rbar final inicio pais, horizontal), scheme(ipea_barhc) xtitle("Início/Fim da década de 90")  ytitle("País") ylabel(1(1)6) xlabel(0.2(0.1)0.7)

// Gráfico 9 - falta fazer
// Load the data ipea
use "\\SBSB2\dpti\Usuarios\Lucas Mation\theme_ipea\dados\base_grafico9.dta", clear

// Default do stata para gráficos de barras e linhas

// Gráfico 9 Ipea - Scheme


// Gráfico 10
// Load the data
use "\\SBSB2\dpti\Usuarios\Lucas Mation\theme_ipea\dados\base_grafico10.dta", clear

// Default do stata para gráficos de barras com linhas
tw (line PIB Data)

// Gerando o scheme para o gráfico 10
brewscheme, scheme(ipea_lineav) allstyle(mdepoint) allcolors(3)

// Default do stata para gráficos de linhas com média
tw (line PIB Data), scheme(ipea_lineav) xtitle("Ano") ytitle(" Cresc. % do PIB") xlabel(1948(5)2013)  
