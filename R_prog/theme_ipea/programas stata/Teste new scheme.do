// Alterando o Caminho ado\personal
sysdir set PERSONAL "D:\Users\b240174\personal"

 adopath

// instalando o brewer no personal
brewcolordb, rep

// Load the auto.dta dataset
sysuse auto.dta, clear

// Loop over the schemes
foreach scheme in onecolorex1 onecolorex2 ggplot2ex1 somecolorex1           ///   
manycolorex1 ggplot2ex2 { 

  // Create the same graph with each of the different schemes
  tw  fpfitci mpg weight ||                                                 ///   
    scatter mpg weight if rep78 == 1 ||                                     ///   
    scatter mpg weight if rep78 == 2 ||                                     ///    
    scatter mpg weight if rep78 == 3 ||                                     ///    
    scatter mpg weight if rep78 == 4 ||                                     ///    
    scatter mpg weight if rep78 == 5,  scheme(`scheme')                     ///   
    legend(order(1 "Frac Poly" 2 "Frac Poly" 3 "1" 4 "2"                    ///   
    5 "3" 6 "4" 7 "5")) name(`scheme', replace)
    
}  // End of Loop over scheme files

// Exemplo único - mixed 1

brewscheme, scheme(mixed1) scatst(set1) barst(pastel1) somest(brbg)
brewscheme, scheme(onecolorex2) allsty(ggplot2) allc(4) themef(s2theme)
gr box mpg, over(rep78) scheme(mixed1)
graph bar mpg, over(rep78) scheme(mixed1)
scatter mpg price, scheme(mixed1)
twoway bar mpg price, scheme(mixed1)

// Using different numbers of colors from the same scheme
// to highlight differences and showing the use of the
// symbols parameter
brewscheme, scheme(ggplot2ex2) const(orange) cone(blue) 
consat(20) scatst(ggplot2) scatc(5) piest(ggplot2) 
piec(6) barst(ggplot2) barc(2) linest(ggplot2) linec(2) 
areast(ggplot2) areac(5) somest(ggplot2) somec(15) 
cist(ggplot2) cic(3) themef(ggtheme) 
symbols(diamond triangle square)


// Com os dados do gráfico 1
use "\\SBSB2\dpti\Usuarios\Lucas Mation\theme_ipea\dados\base_grafico1.dta", clear

brewscheme, scheme(mixed1) scatst(set1) barst(pastel1) somest(brbg)
twoway bar labels Ano, scheme(mixed1)
twoway bar labels Ano, scheme(onecolorex1) xtitle("Ano") ytitle("População")

tw bar labels Ano, ylab(#10, angle(0) nogrid) graphr(ic(white) fc(white) lc(white)) plotr(ic(white) fc(white) lc(white)) xlab(#7) ti("Crescimento População", c(black) span size(medlarge)) xtitle("Ano") ytitle("População") 

// aplicando brewscheme pronto
brewscheme, scheme(onePaletteToRuleThemAll) allst(mdebar)

tw bar labels Ano, scheme(onePaletteToRuleThemAll) ti("Crescimento População", c(black) span size(medlarge)) xtitle("Ano") ytitle("População")

// Exemplos utilizando o pacote - entendimento de como funciona

sysuse nlsw88.dta, clear

tw scatter wage ttl_exp if industry == 11 || scatter wage ttl_exp if industry == 4 || scatter wage ttl_exp if industry == 6 || scatter wage ttl_exp if industry == 7, ti("Not so Simple Scatterplot", c(black) span size(medlarge))

tw scatter wage ttl_exp if industry == 11, mc(red) mlc(black) mlw(vthin) msize(medsmall) || scatter wage ttl_exp if industry == 4, mc(orange) mlc(black) mlw(vthin) msize(medsmall) || scatter wage ttl_exp if industry == 6, mc(yellow) mlc(black) mlw(vthin) msize(medsmall) || scatter wage ttl_exp if industry == 7, mc(green) mlc(black) mlw(vthin) msize(medsmall) ylab(#10, angle(0) nogrid) legend(pos(12) span label(1 "Professional") label(2 "Manufacturing") label(3 "Wholesale/Retail") label(4 "Fin./Insure/Real Estate") rows(1) region(lc(white) fc(white) ic(white)) size(medsmall) symy(2.5) symx(2.5)) graphr(ic(white) fc(white) lc(white)) plotr(ic(white) fc(white) lc(white)) xlab(#7) ti("Not so Simple Scatterplot", c(black) span size(medlarge))

// 1º brewscheme

brewscheme, scheme(onePaletteToRuleThemAll) allst(mdebar)

tw scatter wage ttl_exp if industry == 11 || scatter wage ttl_exp if industry == 4 || scatter wage ttl_exp if industry == 6 || scatter wage ttl_exp if industry == 7, ti("Not so Simple Scatterplot") scheme(onePaletteToRuleThemAll) legend(label(1 "Professional") label(2 "Manufacturing") label(3 "Wholesale/Retail") label(4 "Fin./Insure/Real Estate"))

tw scatter wage ttl_exp if industry == 11 || scatter wage ttl_exp if industry == 4 || scatter wage ttl_exp if industry == 6 || scatter wage ttl_exp if industry == 7 || lfitci wage ttl_exp if industry == 11, ti("Not so Simple Scatterplot") scheme(oneSchemeToRuleThemAll) legend(label(1 "Professional") label(2 "Manufacturing") label(3 "Wholesale/Retail") label(4 "Fin./Insure/Real Estate") rows(3))

// Gerando o primeiro Tema
// construir theme
// Generate the theme file used to simulate ggplot2 aesthetics
brewtheme ggtheme, numticks("major 5" "horizontal_major 5" "vertical_major 5" "horizontal_minor 10" "vertical_minor 10") color("plotregion gs15" "matrix_plotregion gs15" "background gs15" "textbox gs15" "legend gs15"  "box gs15" "mat_label_box gs15" "text_option_fill gs15" "clegend gs15" "histback gs15" "pboxlabelfill gs15" "plabelfill gs15" "pmarkbkfill gs15" "pmarkback gs15") linew("major_grid medthick" "minor_grid thin" "legend medium" "clegend medium") clockdir("legend_position 3") yesno("draw_major_grid yes" "draw_minor_grid yes" "legend_force_draw yes" "legend_force_nodraw no" "draw_minor_vgrid yes" "draw_minor_hgrid yes" "extend_grid_low yes" "extend_grid_high yes" "extend_axes_low no" "extend_axes_high no") gridsty("minor minor") axissty("horizontal_default horizontal_withgrid" "vertical_default vertical_withgrid") linepattern("major_grid solid" "minor_grid solid") linesty("major_grid major_grid" "minor_grid minor_grid") ticksty("minor minor_notick" "minor_notick minor_notick") ticksetsty("major_vert_withgrid minor_vert_nolabel" "major_horiz_withgrid minor_horiz_nolabel" "major_horiz_nolabel major_horiz_default" "major_vert_nolabel major_vert_default") gsize("minortick_label minuscule" "minortick tiny") numsty("legend_cols 1" "legend_rows 0" "zyx2rows 0" "zyx2cols 1") verticaltext("legend top") 
// Create a scheme file using the theme file created with the syntax above
brewscheme, scheme(ggtest2) const(orange) cone(blue) consat(20) scatc(5)    ///  
scatst(ggplot2) piest(ggplot2) piec(6) barst(ggplot2) barc(2) linec(2)      ///   
linest(ggplot2) areast(ggplot2) areac(5) somest(ggplot2) somec(24) cic(3)   ///   
cist(ggplot2) themef(ggplot2)

// Create a graph with the scheme file above
tw  lowess mpg weight ||                                                    ///   
    scatter mpg weight if rep78 == 1 ||                                     ///   
    scatter mpg weight if rep78 == 2 ||                                     ///   
    scatter mpg weight if rep78 == 3 ||                                     ///   
    scatter mpg weight if rep78 == 4 ||                                     ///   
    scatter mpg weight if rep78 == 5,                                       ///   
    legend(order(2 "1978 Repair Record = 1"                                 ///   
    3 "1978 Repair Record = 2" 4 "1978 Repair Record = 3"                   ///   
    5 "1978 Repair Record = 4" 6 "1978 Repair Record = 5"))                 ///   
    scheme(ggtest2) 
	
// Gerando exemplo simples de gráficos
// Load the auto data set
sysuse auto, clear

// Create a new scheme file for all graph types
brewscheme, scheme(ipea) allstyle(dark2) allcolor(7)

// Make a simple graph using Stata defaults
tw scatter mpg length || lfit mpg length, name(stata, replace)


// Make the same graph using the scheme file you just created
tw scatter mpg length || lfit mpg length, scheme(ipea) name(brewscheme, replace)
	

// Gerando o promeoro theme para o gráfico 1
// Load the data ipea
use "\\SBSB2\dpti\Usuarios\Lucas Mation\theme_ipea\dados\base_grafico1.dta", clear

brewscheme, scheme(ipea) allstyle(mdepoint) allcolor(3) 

// Make a simple graph ipea
tw bar labels Ano, scheme(ipea)  yscale(range(0 10) titlegap(1)) xscale(range(2000 2050) titlegap(5)) ti("Crescimento População", c(black) span size(medlarge)) xtitle("Ano") ytitle("População")

tw bar labels Ano, scheme(ipea)  yscale(range("0" "10") titlegap(1)) xscale(range(2000 2050) titlegap(5)) ti("Crescimento População", c(black) span size(medlarge)) xtitle("Ano") ytitle("População")

// Gerando o theme Ipea

brewtheme theme_ipea,  col("axis_title black" "axisline black" "background white") graphsty ("graph") gridsty("minor minor") axissty("horizontal_default horizontal_withgrid" "vertical_default vertical_withgrid")


brewscheme, scheme(theme_ipea) barst(darknavy) themef(theme_ipea)

tw bar Populacao Ano, scheme(theme_ipea)

